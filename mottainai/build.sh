#!/bin/bash
env

mkdir -p $GOPATH/src/github.com/MottainaiCI/

ln -s $(pwd)/mottainai-cli $GOPATH/src/github.com/MottainaiCI/mottainai-cli

go get github.com/urfave/cli
go get github.com/MottainaiCI/mottainai-server
go get github.com/mitchellh/gox

echo "Building mottainai-cli"
git clone https://github.com/MottainaiCI/mottainai-cli
pushd mottainai-cli
	make build
	cp -rfv release/* ../artefacts/
popd

go get github.com/MottainaiCI/mottainai-agent
git clone https://github.com/MottainaiCI/mottainai-agent
pushd mottainai-agent
	make build
	cp -rfv release/* ../artefacts/
popd

git clone https://github.com/MottainaiCI/mottainai-server
pushd mottainai-server
	make build
	cp -rfv release/* ../artefacts/
popd

echo "Done"
